from app import app
from app.ocr.extract import initOCR

if __name__ == '__main__':
	initOCR()
	app.run(debug=True)
