import pandas as pd
import numpy as np

def levenshtein_ratio_and_distance(s, t, ratio_calc=False):

    rows = len(s) + 1
    cols = len(t) + 1
    distance = np.zeros((rows, cols), dtype=int)

    for i in range(1, rows):
        for k in range(1, cols):
            distance[i][0] = i
            distance[0][k] = k

    for col in range(1, cols):
        for row in range(1, rows):
            if s[row - 1] == t[col - 1]:
                cost = 0
            else:
                if ratio_calc == True:
                    cost = 2
                else:
                    cost = 1
            distance[row][col] = min(distance[row - 1][col] + 1,
                                     distance[row][col - 1] + 1,
                                     distance[row - 1][col - 1] + cost)
    if ratio_calc == True:
        Ratio = ((len(s) + len(t)) - distance[row][col]) / (len(s) + len(t))
        return Ratio
    else:
        return "The strings are {} edits away".format(distance[row][col])


def mat_dist(lista_compara, text_palabras):
    return pd.DataFrame(
        [[levenshtein_ratio_and_distance(k.lower(), palabras.lower(), ratio_calc=True) for palabras in text_palabras]
         for k in lista_compara])


def getIndexDF(df, numberToFind):
    tam_df = df.shape
    for ren in range(0, tam_df[0]):
        for col in range(1, tam_df[1]):
            if df.iloc[ren, col] == numberToFind:
                return ren, col


def getValMaxDF(df):
    df = df.max()
    df = df.max()
    return df


def funcFindText(text_pal):
    paises = ["mxn", "mexicanos", "colombianos", "chilenos", "peruanos", "e.u.a", "americanos", "estadounidenses"]
    multiplos = ["miles", "millones"]
    divisas = ["pesos", "dolares", "colones", "dólares", "usd", "euros"]

    mat_pais = mat_dist(paises, text_pal)
    mat_mult = mat_dist(multiplos, text_pal)
    mat_div = mat_dist(divisas, text_pal)

    paisMaxValDF = getValMaxDF(mat_pais)
    multMaxValDF = getValMaxDF(mat_mult)
    divMaxValDF = getValMaxDF(mat_div)

    indexPaisDF = getIndexDF(mat_pais, paisMaxValDF)
    indexMultDF = getIndexDF(mat_mult, multMaxValDF)
    indexDivDF = getIndexDF(mat_div, divMaxValDF)

    pais = ""
    multiplo = ""
    divisa = ""

    minValAllow = 0.8

    if paisMaxValDF > minValAllow:
        try:
            pais = paises[indexPaisDF[0]]
        except:
            pais = ""
    else:
        pais = ""

    if multMaxValDF > minValAllow:
        try:
            multiplo = multiplos[indexMultDF[0]]
        except:
            multiplo = ""
    else:
        multiplo = ""

    if divMaxValDF > minValAllow:
        try:
            divisa = divisas[indexDivDF[0]]
        except:
            divisa = ""
    else:
        divisa = ""

    if divisa == "usd":
        divisa = "dolares"
        pais = "estadounidenses"

    if pais == "mxn":
        pais = "mexicanos"

    if pais == "e.u.a." or pais == "e.u.a":
        pais = "estadounidenses"

    return str(multiplo) + " " + str(divisa) + " " + str(pais)


from django.contrib.admin.utils import flatten


def getValsMaxDF(df):
    """  """
    df = df.max()
    return df


def indexMaxDist(list_text, list_comp, dist_min):
    ind = []
    mat_comp = mat_dist(list_comp, list_text)
    tam_list = len(list_text)
    compMax_vec = getValsMaxDF(mat_comp)
    for i in range(tam_list):
        if compMax_vec[i] > dist_min:
            ind.append(i)
    return ind


def most_frequent(List):
    counter = 0
    num = List[0]

    for i in List:
        curr_frequency = List.count(i)
        if (curr_frequency > counter):
            counter = curr_frequency
            num = i
    return num


def findDateText(text_pal):
    """ """
    meses = ["diciembre", "noviembre", "octubre", "septiembre", "agosto", "julio", "junio", "mayo", "abril", "marzo",
             "febrero", "enero"]
    anios_int = list(range(2000, 2021))
    anios = [" " + str(x) + " " for x in anios_int]
    dias_int = list(range(1, 32))
    dias = [" " + str(x) + " " for x in dias_int]

    tam_vent = 4
    fechas = []
    ind_mes = indexMaxDist(text_pal, meses, 0.8)
    for i in ind_mes:
        ventana = text_pal[max(0, i - tam_vent): min(len(text_pal), i + tam_vent)]
        ind_anios_vent = indexMaxDist(ventana, anios, 0.6)
        ind_dias_vent = indexMaxDist(ventana, dias, 0.6)
        val_dias = [text_pal[j + i - tam_vent] for j in ind_dias_vent]
        val_anios = [text_pal[j + i - tam_vent] for j in ind_anios_vent]
        fechas.append([val_dias, text_pal[i], val_anios])
    fecha_df = pd.DataFrame(fechas)

    d_vecs = flatten(fecha_df[0])
    m_vecs = flatten(fecha_df[1])
    a_vecs = flatten(fecha_df[2])
    print(fecha_df)
    return str(most_frequent(d_vecs)) + " " + str(most_frequent(m_vecs)).lower() + " " + str(most_frequent(a_vecs))


def getDateText(palabras):
    try:
        return findDateText(palabras)
    except:
        return "Na"


from operator import itemgetter
from itertools import chain


def split_list(lectura_sorted, y_jump_index):
    # print('#######lectura_sorted',lectura_sorted)
    # print('#######yjump',y_jump_index)
    for r in range(len(lectura_sorted)):
        lectura_sorted[r].extend(
            [lectura_sorted[r][3] - lectura_sorted[r][1], lectura_sorted[r][6] - lectura_sorted[r][4]])
    for r in range(len(lectura_sorted)):
        if r > 0:
            lectura_sorted[r].append(lectura_sorted[r][8] - lectura_sorted[r - 1][8])
        else:
            lectura_sorted[r].append(0)
    for r in range(len(lectura_sorted)):
        if r > 0:
            try:
                if lectura_sorted[r][11] / lectura_sorted[r - 1][10] >= y_jump_index:
                    lectura_sorted[r].append('jump')
                else:
                    lectura_sorted[r].append('')
            except Exception:
                lectura_sorted[r].append('')
        else:
            lectura_sorted[r].append('')

    jumps = [lectura_sorted.index(i) for i in lectura_sorted if i[-1] == 'jump']
    temp = zip(chain([0], jumps), chain(jumps, [None]))
    global res
    res = [lectura_sorted[i:j] for i, j in temp]
    for r in range(len(res)):
        for q in range(len(res[r])):
            res[r][q] = res[r][q][:9]
    for i in range(len(res)):
        res[i] = sorted(res[i], key=itemgetter(1, 0))
    return res