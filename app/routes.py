import os
import time
from app import app
from flask import render_template, request, redirect, url_for, current_app, send_file, request, jsonify, Response
from app.forms import UploadForm
import requests
import app.ocr.extract as ext
import app.divisas as divs
import pandas as pd
import json

curret_data = []
table_content = []
@app.route('/', methods=['GET', 'POST'])
def index():
    form = UploadForm()
    if form.validate_on_submit():
        file = form.file.data
        filename = file.filename
        ext_type = filename.split('.')[-1]
        output = time.strftime("%d%m%y-%H%M%S") + '.' + ext_type
        file.save(os.path.join(current_app.root_path, 'user/uploads', output))

        global preview, ocr_data, current_file, divisa

        current_file = os.path.join(current_app.root_path, 'user/uploads', output)

        ocr_data=ext.get_all(current_file)

        ocr_data = ocr_data['data'][0]
        ocr_data = jsonTransformer(ocr_data, filename)
        headers = ocr_data.keys()

        quieroserunmaestropokemon=[]
        #for a in ext.get_lecture(current_file, True):
            #quieroserunmaestropokemon.append(a[0])

        quieroserunmaestropokemon=ext.get_lecture(current_file, True)

        y_jump_index = 0.9

        res=divs.split_list(quieroserunmaestropokemon, y_jump_index)

        new_list = []

        for i in res:
            new_list.extend(i)

        new_list = [i[0] for i in new_list if ' ' not in i[0]]


        divisa=divs.funcFindText(new_list)
        fecha=divs.getDateText(new_list)
        ocr_data["fecha"]=fecha
        ocr_data['unidades de medida'] = divisa

        return render_template('index.html',
            form=form,
            #preview=preview,
            ocr_data=ocr_data,
            addButton=True,
            headers=headers,
            data=curret_data,
        )

    global table

    if request.method == 'POST':
        if request.form['submit_button'] == 'Agregar':
            registro = ocr_data
            # registro['unidades de medida'] = divisa
            headers = registro.keys()
            curret_data.append(registro.values())
            table_content.append(list(registro.values()))


            table=buildJsonTable(list(headers), table_content)

        return render_template('index.html',
            form=form,
            #preview=preview,
            ocr_data=ocr_data,
            headers=headers,
            data=curret_data,
            addButton=False,
        )
    return render_template('index.html',form=form)


@app.route('/descarga-<string:extension>/')
def download(extension):
    archivo = ''
    df = pd.DataFrame.from_dict(table)
    if extension == 'csv':
        return Response(
            df.to_csv(index=False),
            mimetype="text/csv",
            headers={"Content-disposition":
            "attachment; filename=filename.csv"})
    elif extension == 'txt':
        return Response(
            df.to_csv(index=False),
            mimetype="text/csv",
            headers={"Content-disposition":
            "attachment; filename=filename.txt"})
    elif extension == 'xlsx':
        return Response(
            df.to_excel(index=False),
            mimetype="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            headers={"Content-disposition":
            "attachment; filename=filename.xlsx"})
    else:
        return 'error'


def buildJsonTable(headers, table_content):
    table = {}
    table['docucmento'] = [item[0] for item in table_content]
    table['fecha'] = [item[1] for item in table_content]
    table['unidades de medida'] = [item[2] for item in table_content]
    table['caja y efectivo'] = [item[3] for item in table_content]
    table['total activo'] = [item[4] for item in table_content]
    table['total pasivo'] = [item[5] for item in table_content]
    table['total patrimonio'] = [item[6] for item in table_content]
    table['ventas'] = [item[7] for item in table_content]
    table['costo de ventas'] = [item[8] for item in table_content]
    table['utilidad bruta'] = [item[9] for item in table_content]
    table['utilidad operacional'] = [item[10] for item in table_content]
    table['utilidad antes de impuestos'] = [item[11] for item in table_content]
    table['utilidad neta'] = [item[12] for item in table_content]

    return table


data = []
@app.route('/json-table/', methods=['GET', 'POST'])
def jsonTable():
    if request.method == 'GET':
        return jsonify({'metodo':'GET'})

    if request.method == 'POST':
        json_in = request.json['documento']
        # out = {}
        out = {
            "id": "",
            "caja y efectivo": "",
            "total activo": "",
            "total pasivo": "",
            "total patrimonio": "",
            "ventas": "",
            "costo de ventas": "",
            "utilidad bruta": "",
            "utilidad operacional": "",
            "utilidad antes de impuestos": "",
            "utilidad neta": "",

            "fecha": "",
            "unidades de medida": "",
        }
        for balance in json_in['balance general']:
            out[balance] = json_in['balance general'][balance]
        for estados in json_in['estados perdidas y ganancias']:
            out[estados] = json_in['estados perdidas y ganancias'][estados]
        for general in json_in['general']:
            out[general] = json_in['general'][general]
        # print(out)
        # return jsonify(out)
        headers = out.keys()
        data.append(out.values())
        # print(data)
        # return render_template('jsontable.html', headers=headers)
        # https://stackoverflow.com/questions/14652325/python-dictionary-in-to-html-table/14656262
        return render_template('jsontable.html', headers=headers, data=data)


@app.route('/json-transformer/', methods=['GET', 'POST'])
def jsonEjemplo():
    if request.method == 'GET':
        return jsonify({'metodo':'GET'})
    out={}
    if request.method == 'POST':
        datos = request.json['documento']
        for balance in datos['balance general']:
            out[balance] = datos['balance general'][balance]
        for estado in datos['estados perdidas y ganancias']:
            out[estado] = datos['estados perdidas y ganancias'][estado]
        for general in datos['general']:
            out[general] = datos['general'][general]
        print(out)
        return jsonify(out)


def jsonTransformer(data, filename):
    out = {
        "documento": "",

        "fecha": "",
        "unidades de medida": "",
        "caja y efectivo": "",
        "total activo": "",
        "total pasivo": "",
        "total patrimonio": "",

        "ventas": "",
        "costo de ventas": "",
        "utilidad bruta": "",
        "utilidad operacional": "",
        "utilidad antes de impuestos": "",
        "utilidad neta": "",
    }
    # data = data['documento']
    out['documento'] = str(filename)
    for balance in data['balance general']:
        if data['balance general'][balance] != '':
            out[balance] = data['balance general'][balance][0]
        else:
            out[balance] = 'Na'
    for estado in data['estados perdidas y ganancias']:
        if data['estados perdidas y ganancias'][estado] != '':
            out[estado] = data['estados perdidas y ganancias'][estado][0]
        else:
            out[estado] = 'Na'
    for general in data['general']:
        if data['general'][general] != '':
            out[general] = data['general'][general][0]
        else:
            out[general] = 'Na'

    return out
