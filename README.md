# Virtual Environment:

	pip install -r requirements.txt

# Run App

	set FLASK_APP=app.py (Windows)
	export FLASK_APP=app.py (Linux/MacOS)

	python app.py
